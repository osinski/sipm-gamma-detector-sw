/*
 * sca.cc
 * Copyright (C) 2021 Marcin Osiński <osinski.marcin.r@gmail.com>
 *
 * Distributed under terms of the MIT license.
 */

#include "sca.h"

#include <cstdint>
#include <string>

#include "../tivaWare/driverlib/i2c.h"
#include "../tivaWare/driverlib/timer.h"
#include "../tivaWare/driverlib/gpio.h"
#include "../tivaWare/driverlib/pin_map.h"
#include "../tivaWare/driverlib/rom.h"
#include "../tivaWare/driverlib/rom_map.h"
#include "../tivaWare/inc/hw_memmap.h"
#include "../tivaWare/inc/hw_types.h"
#include "../tivaWare/inc/hw_timer.h"

#include "uart.h"

extern UART uart;

namespace SCA {
    constexpr static auto i2c = I2C3_BASE;
    constexpr static auto i2cAddr = 0b010'1111;
    constexpr static auto fullCode = 256;
    constexpr static auto timer = TIMER2_BASE;
    constexpr static auto timerGpioPort = GPIO_PORTM_BASE;
    constexpr static auto timerGpioPin = GPIO_PIN_0;
    constexpr static auto timerGpioFunction = GPIO_PM0_T2CCP0;

    volatile static bool enabled;
    static SCA::Data data;

    inline void
    enable()
    {
        MAP_GPIOPinConfigure(timerGpioFunction);
        MAP_GPIOPinTypeTimer(timerGpioPort, timerGpioPin);

        MAP_TimerConfigure(timer, (TIMER_CFG_SPLIT_PAIR |
                                   TIMER_CFG_A_CAP_COUNT_UP |
                                   TIMER_CFG_B_PERIODIC |
                                   TIMER_CFG_B_ACT_NONE));
        MAP_TimerControlEvent(timer, TIMER_A, TIMER_EVENT_POS_EDGE);
        MAP_TimerMatchSet(timer, TIMER_A, 0xFFFF);
        MAP_TimerPrescaleSet(timer, TIMER_A, 0xFF);
        HWREG(timer + TIMER_O_TAR) = 0;
        HWREG(timer + TIMER_O_TAV) = 0;
        MAP_TimerEnable(timer, TIMER_A);

        enabled = true;

        data.first = 0;
        data.second = 0;
    }

    inline void
    disable()
    {
        MAP_TimerDisable(timer, TIMER_A);

        MAP_GPIOPinTypeGPIOInput(timerGpioPort, timerGpioPin);

        enabled = false;
    }

    inline void
    setNewLLD(const std::uint_fast16_t newLLDmilivolts)
    {
        constexpr auto minLLD = 0;
        constexpr auto maxLLD = 1200;
        constexpr static uint8_t instructionByte = (1 << 7) | (0 << 6) | 0x00;


        uint8_t code;

        if (newLLDmilivolts > minLLD && newLLDmilivolts < maxLLD) {
            code = static_cast<float>(newLLDmilivolts - minLLD) / 
                   static_cast<float>(maxLLD - minLLD) * fullCode;
        } else {
            code = 255;
        }

        uart.sendString("DigiPot code for this LLD is: ");
        uart.sendString(std::to_string(code).c_str());
        uart.sendString("\r\n");

        MAP_I2CMasterSlaveAddrSet(i2c, i2cAddr, false);
        MAP_I2CMasterDataPut(i2c, instructionByte);
        MAP_I2CMasterControl(i2c, I2C_MASTER_CMD_BURST_SEND_START);

        while (MAP_I2CMasterBusy(i2c)) {}

        MAP_I2CMasterDataPut(i2c, code);
        MAP_I2CMasterControl(i2c, I2C_MASTER_CMD_BURST_SEND_FINISH);

        while (MAP_I2CMasterBusy(i2c)) {}
    }

    inline void
    setNewULD(const std::uint_fast16_t newULDmilivolts)
    {
        constexpr auto minULD = 0;
        constexpr auto maxULD = 1200;
        constexpr static uint8_t instructionByte = (0 << 7) | (0 << 6) | 0x00;

        uint8_t code;

        if (newULDmilivolts > minULD && newULDmilivolts < maxULD) {
            code = static_cast<float>(newULDmilivolts - minULD) /
                   static_cast<float>(maxULD - minULD) * fullCode;
        } else {
            code = 255;
        }

        uart.sendString("DigiPot code for this ULD is: ");
        uart.sendString(std::to_string(code).c_str());
        uart.sendString("\r\n");

        MAP_I2CMasterSlaveAddrSet(i2c, i2cAddr, false);
        MAP_I2CMasterDataPut(i2c, instructionByte);
        MAP_I2CMasterControl(i2c, I2C_MASTER_CMD_BURST_SEND_START);

        while (MAP_I2CMasterBusy(i2c)) {}

        MAP_I2CMasterDataPut(i2c, code);
        MAP_I2CMasterControl(i2c, I2C_MASTER_CMD_BURST_SEND_FINISH);

        while (MAP_I2CMasterBusy(i2c)) {}
    }

    inline std::uint_fast64_t
    getCounts()
    {
        // TODO make it more reliable
        static auto lastVal = 0;

        std::uint_fast64_t counts = MAP_TimerValueGet(timer, TIMER_A);

        if (counts < lastVal) {
            counts += 0x00;
        }

        lastVal = counts;    

        return counts;
    }

};


extern "C" {

QueueHandle_t queueSCAMsgs;
QueueHandle_t queueSCAData;

void
taskSCAConfig(void*)
{
    static SCA::Message msgSCA;

    queueSCAMsgs = xQueueCreate(1, sizeof(SCA::Message));

    while (true) {
        xQueueReceive(queueSCAMsgs, &msgSCA, portMAX_DELAY);
        switch (msgSCA.first) {
        case SCA::MessageType::NEW_LLD:
        {
            SCA::setNewLLD(msgSCA.second);
            break;
        }
        case SCA::MessageType::NEW_ULD:
        {
            SCA::setNewULD(msgSCA.second);
            break;
        }
        case SCA::MessageType::ENABLE:
        {
            SCA::enable();
            break;
        }
        case SCA::MessageType::DISABLE:
        {
            SCA::disable();
            break;
        }
        case SCA::MessageType::SEND_DATA:
        {
            xQueueSend(queueSCAData, &SCA::data, 0);
            break;
        }
        }
    }
}

void
taskSCA(void*)
{
    static std::uint_fast16_t prevCounts = 0;

    queueSCAData = xQueueCreate(1, sizeof(SCA::Data));

    while (true) {
        if (SCA::enabled) {
            vTaskDelay(pdMS_TO_TICKS(1000));

            prevCounts = SCA::data.first;
            SCA::data.first = SCA::getCounts();
            SCA::data.second = SCA::data.first - prevCounts;
        }
    }
}


}

