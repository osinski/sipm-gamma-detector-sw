/*
 * debug.c
 * Copyright (C) 2021 Marcin Osiński <osinski.marcin.r@gmail.com>
 *
 * Distributed under terms of the MIT license.
 */

#include <stdint.h>
#include <string>

#include "uart.h"
#include "cmdline.h"

extern UART uart;

extern "C" {

#ifdef DEBUG
// Used internally by driverlib
void __error__(char *pcFilename, uint32_t ui32Line)
{
    uart.sendString("\r\n\r\n");
    uart.sendString(ANSI_EscapeCodes::Colors::red);
    uart.sendString(ANSI_EscapeCodes::Decorations::bold);
    uart.sendString("DRIVERLIB ERROR!\r\n");
    uart.sendString("File: ");
    uart.sendString(pcFilename);
    uart.sendString("\r\nLine: ");
    uart.sendString(std::to_string(ui32Line).c_str());
    uart.sendString("\r\n");

    while(1);
}
#endif

}
