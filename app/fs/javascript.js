var scaOn = false;
var mcaOn = false;
var scaULD = 800;
var scaLLD = 200;
var scaCounts = "0";
var scaCPS = "0";
var biasOV = 1000;
var measMinutes = 0;

window.onload = function() {
    document.getElementById("about").onclick = loadAbout;
    document.getElementById("bias").onclick = loadBiasCtrl;
    document.getElementById("sca").onclick = loadSCA;
    document.getElementById("mca").onclick = loadMCA;

    loadAbout();
    document.getElementById("plotMCA").style.display = "none";
    document.getElementById("plotSCA").style.display = "none";

    window.setInterval(updatePlots, 2500);
}

function loadAbout() {
    loadPage("about.html");
    document.getElementById("plotMCA").style.display = "none";
    document.getElementById("plotSCA").style.display = "none";

    return false;
}

function loadBiasCtrl() {
    loadPage("biasctrl.html");
    document.getElementById("plotMCA").style.display = "none";
    document.getElementById("plotSCA").style.display = "none";

    return false;
}

function loadSCA() {
    loadPage("sca.html")
    document.getElementById("plotMCA").style.display = "none";
    document.getElementById("plotSCA").style.display = "inline-block";

    return false;
}

function loadMCA() {
    loadPage("mca.html");
    document.getElementById("plotMCA").style.display = "inline-block";
    document.getElementById("plotSCA").style.display = "none";

    return false;
}

function loadPage(page) {
    if (window.XMLHttpRequest) {
        xmlhttp = new XMLHttpRequest();
    } else {
        xmlhttp = new ActiveXObject("Microsoft.XMLHTTP");
    }

    xmlhttp.open("GET", page, true);
    xmlhttp.setRequestHeader("Content-type",
        "application/x-www-form-urlencoded");
    xmlhttp.send();

    xmlhttp.onreadystatechange = function() {
        if ((xmlhttp.readyState == 4) && (xmlhttp.status == 200)) {
            document.getElementById("content").innerHTML = xmlhttp.responseText;

            if (page === "sca.html") {
                if (scaOn) {
                    document.getElementById("scaOnOff").checked=true;
                }
                document.getElementById("sca_lld").value = scaLLD;
                document.getElementById("sca_uld").value = scaULD;
                document.getElementById("sca-counts").innerHTML = scaCounts;
                document.getElementById("sca-cps").innerHTML = scaCPS;

            } else if (page === "mca.html") {
                if (mcaOn) {
                    document.getElementById("mcaOnOff").checked=true;
                }

            } else if (page === "biasctrl.html") {
                document.getElementById("bias-ov").value = biasOV;
            }
        }
    }
}

function newLLD(val) {
    var req = false;

    if (window.XMLHttpRequest) {
        req = new XMLHttpRequest();
    } else if (window.ActiveXObject) {
        req = new ActiveXObject("Microsoft.XMLHTTP");
    }

    if (req) {
        req.open("GET", "newLLD=" + val, true);
        req.onreadystatechange = function() {
            if (req.readyState == 4 && req.status == 200) {
                if (req.responseText == "ACK") {

                } else if (req.responseText == "NACK") {

                } else {

                }
            }
        }
        req.send();
    }
}

function newULD(val) {
    var req = false;

    if (window.XMLHttpRequest) {
        req = new XMLHttpRequest();
    } else if (window.ActiveXObject) {
        req = new ActiveXObject("Microsoft.XMLHTTP");
    }

    if (req) {
        req.open("GET", "newULD=" + val, true);
        req.onreadystatechange = function() {
            if (req.readyState == 4 && req.status == 200) {
                if (req.responseText == "ACK") {

                } else if (req.responseText == "NACK") {

                } else {

                }
            }
        }
        req.send();
    }
}

function newOV(val) {
    if (event.code === "Enter") {
        var req = false;
        biasOV = val.value;

        if (window.XMLHttpRequest) {
            req = new XMLHttpRequest();
        } else if (window.ActiveXObject) {
            req = new ActiveXObject("Microsoft.XMLHTTP");
        }

        if (req) {
            req.open("GET", "newOV=" + biasOV, true);
            req.onreadystatechange = function() {
                if (req.readyState == 4 && req.status == 200) {
                    if (req.responseText == "ACK") {

                    } else if (req.responseText == "NACK") {

                    } else {

                    }
                }
            }
            req.send();
        }
    }
}

function onOffSCA() {
    var reqSCA = false;
    var reqMCA = false;

    if (window.XMLHttpRequest) {
        reqSCA = new XMLHttpRequest();
    } else if (window.ActiveXObject) {
        reqSCA = new ActiveXObject("Microsoft.XMLHTTP");
    }

    if (document.getElementById("scaOnOff").checked) {
        scaOn = true;
        scaCounts = "0";
        scaCPS = "0";

        if (reqSCA) {
            reqSCA.open("GET", "SCA=ON", true);
            reqSCA.onreadystatechange = function() {
                if (reqSCA.readyState == 4 && reqSCA.status == 200) {
                    if (reqSCA.responseText == "ACK") {

                    } else if (reqSCA.responseText == "NACK") {
                        alert("SCA=ON got NACK");
                    } else {
                        alert("SCA=ON got unknown response");
                    }
                }
            }
            reqSCA.send();
        }

        Plotly.restyle(plotSCADiv, {y: [[0]]});

        var x = window.setInterval(function() {
            measMinutes = document.getElementById("meas_time").value

            if (measMinutes == 1) {
                window.clearInterval(x);
                scaOn = false;
                document.getElementById("scaOnOff").checked = false;
                if (window.XMLHttpRequest) {
                    reqSCA = new XMLHttpRequest();
                } else if (window.ActiveXObject) {
                    reqSCA = new ActiveXObject("Microsoft.XMLHTTP");
                }

                if (reqSCA) {
                    reqSCA.open("GET", "SCA=OFF", true);
                    reqSCA.onreadystatechange = function() {
                        if (reqSCA.readyState == 4 && reqSCA.status == 200) {
                            if (reqSCA.responseText == "ACK") {

                            } else if (reqSCA.responseText == "NACK") {
                                alert("SCA=OFF got NACK");
                            } else {
                                alert("SCA=OFF got unknown response");
                            }
                        }
                    }
                    reqSCA.send();
                }
            } else {
                measMinutes = measMinutes - 1;
                document.getElementById("meas_time").value = measMinutes;
            }
        }, 60*1000);

        newLLD(document.getElementById("sca_lld").value);
        newULD(document.getElementById("sca_uld").value);

        if (mcaOn) {
            mcaOn = false;
            if (window.XMLHttpRequest) {
                reqMCA = new XMLHttpRequest();
            } else if (window.ActiveXObject) {
                reqMCA = new ActiveXObject("Microsoft.XMLHTTP");
            }

            if (reqMCA) {
                reqMCA.open("GET", "MCA=OFF", true);
                reqMCA.onreadystatechange = function() {
                    if (reqMCA.readyState == 4 && reqMCA.status == 200) {
                        if (reqMCA.responseText == "ACK") {

                        } else if (reqMSCA.responseText == "NACK") {
                            alert("MCA=OFF got NACK");
                        } else {
                            alert("MCA=OFF got unknown response");
                        }
                    }
                }
                reqMCA.send();
            }
        }

    } else {
        scaOn = false;
        window.clearInterval(x);

        if (reqSCA) {
            reqSCA.open("GET", "SCA=OFF", true);
            reqSCA.onreadystatechange = function() {
                if (reqSCA.readyState == 4 && reqSCA.status == 200) {
                    if (reqSCA.responseText == "ACK") {

                    } else if (reqSCA.responseText == "NACK") {
                        alert("SCA=OFF got NACK");
                    } else {
                        alert("SCA=OFF got unknown response");
                    }
                }
            }
            reqSCA.send();
        }
    }
}

function onOffMCA() {
    var reqMCA = false;
    var reqSCA = false;

    if (window.XMLHttpRequest) {
        reqMCA = new XMLHttpRequest();
    } else if (window.ActiveXObject) {
        reqMCA = new ActiveXObject("Microsoft.XMLHTTP");
    }

    if (document.getElementById("mcaOnOff").checked) {
        mcaOn = true;

        if (reqMCA) {
            reqMCA.open("GET", "MCA=ON", true);
            reqMCA.onreadystatechange = function() {
                if (reqMCA.readyState == 4 && reqMCA.status == 200) {
                    if (reqMCA.responseText == "ACK") {

                    } else if (reqMCA.responseText == "NACK") {
                        alert("MCA=ON got NACK");
                    } else {
                        alert("MCA=ON got unknown response");
                    }
                }
            }
            reqMCA.send();
        }

        Plotly.restyle(plotMCADiv, {y: [[...Array(1025).fill(0)]]});

        var x = window.setInterval(function() {
            measMinutes = document.getElementById("meas_time").value
    
            if (measMinutes == 1) {
                window.clearInterval(x);
                mcaOn = false;
                document.getElementById("mcaOnOff").checked = false;
                if (window.XMLHttpRequest) {
                    reqSCA = new XMLHttpRequest();
                } else if (window.ActiveXObject) {
                    reqSCA = new ActiveXObject("Microsoft.XMLHTTP");
                }

                if (reqSCA) {
                    reqSCA.open("GET", "MCA=OFF", true);
                    reqSCA.onreadystatechange = function() {
                        if (reqSCA.readyState == 4 && reqSCA.status == 200) {
                            if (reqSCA.responseText == "ACK") {

                            } else if (reqSCA.responseText == "NACK") {
                                alert("MCA=OFF got NACK");
                            } else {
                                alert("MCA=OFF got unknown response");
                            }
                        }
                    }
                    reqSCA.send();
                }
            } else {
                measMinutes = measMinutes - 1;
                document.getElementById("meas_time").value = measMinutes;
            }
        }, 60*1000);

        newLLD(document.getElementById("mca_lld").value);
        newULD(1250);

        if (scaOn) {
            scaOn = false;
            if (window.XMLHttpRequest) {
                reqSCA = new XMLHttpRequest();
            } else if (window.ActiveXObject) {
                reqSCA = new ActiveXObject("Microsoft.XMLHTTP");
            }

            if (reqSCA) {
                reqSCA.open("GET", "SCA=OFF", true);
                reqSCA.onreadystatechange = function() {
                    if (reqSCA.readyState == 4 && reqSCA.status == 200) {
                        if (reqSCA.responseText == "ACK") {

                        } else if (reqSCA.responseText == "NACK") {
                            alert("SCA=OFF got NACK");
                        } else {
                            alert("SCA=OFF got unknown response");
                        }
                    }
                }
                reqSCA.send();
            }
        }

    } else {
        mcaOn = false;
        window.clearInterval(x);

        if (reqMCA) {
            reqMCA.open("GET", "MCA=OFF", true);
            reqMCA.onreadystatechange = function() {
                if (reqMCA.readyState == 4 && reqMCA.status == 200) {
                    if (reqMCA.responseText == "ACK") {

                    } else if (reqMCA.responseText == "NACK") {
                        alert("MCA=ON got NACK");
                    } else {
                        alert("MCA=ON got unknown response");
                    }
                }
            }
            reqMCA.send();
        }
    }
}

function updatePlots() {
    var req = false;
    var recvData = [];

    if (window.XMLHttpRequest) {
        req = new XMLHttpRequest();
    } else if (window.ActiveXObject) {
        req = new ActiveXObject("Microsoft.XMLHTTP");
    }

    if (req) {
        if (mcaOn) {
            req.open("GET", "DataMCA?", true);
            req.onreadystatechange = function () {
                if ((req.readyState == 4) && (req.status == 200)) {
                    recvData = req.responseText.split(",").map(Number);
                    Plotly.restyle(plotMCADiv, {y: [recvData]});
                }
            }
            req.send();
        }
        if (scaOn) {
            req.open("GET", "DataSCA?", true);
            req.onreadystatechange = function () {
                if ((req.readyState == 4) && (req.status == 200)) {
                    recvData = req.responseText.split(",").map(Number);
                    scaCounts = recvData[0];
                    scaCPS = recvData[1];
                    Plotly.extendTraces(plotSCADiv, {y: [[recvData[1]]]}, [0]);
                    document.getElementById("sca-counts").innerHTML = String(scaCounts);
                    document.getElementById("sca-cps").innerHTML = String(scaCPS);
                }
            }
            req.send();
        }
    }
}


