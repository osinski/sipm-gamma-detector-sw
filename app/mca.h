/*
 * mca.h
 * Copyright (C) 2021 Marcin Osiński <osinski.marcin.r@gmail.com>
 *
 * Distributed under terms of the MIT license.
 */

#ifndef MCA_H
#define MCA_H

#include <cstdint>
#include <array>

#include "../freeRTOS/include/FreeRTOS.h"
#include "../freeRTOS/include/queue.h"

namespace MCA {

using SpectrogramData = std::array<std::uint_fast16_t, 1024>;

enum class MessageType
{
    ENABLE,
    DISABLE,
};


}

extern "C" {

extern QueueHandle_t queueMCASpectrogram;
extern QueueHandle_t queueMCAMsgs;

void
taskMCA(void*);

void
taskMCAConfig(void*);

}

#endif /* MCA_H */

