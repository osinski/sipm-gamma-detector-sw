/*
 * sipmOV.h
 * Copyright (C) 2021 Marcin Osiński <osinski.marcin.r@gmail.com>
 *
 * Distributed under terms of the MIT license.
 */

#ifndef SIPMOV_H
#define SIPMOV_H

#include <cstdint>

#include "../tivaWare/driverlib/i2c.h"
#include "../tivaWare/driverlib/rom_map.h"
#include "../tivaWare/inc/hw_memmap.h"

static inline void
setSiPMOvervoltage(const std::uint_fast16_t milivoltsOV)
{
    constexpr auto i2cAddr = 0b001'0110;
    constexpr auto I2C = I2C3_BASE;
    constexpr auto maxOV = 5000;
    constexpr auto fullCode = 127;

    uint8_t code;

    if (milivoltsOV > maxOV) {
        code = fullCode;
    } else {
        code = static_cast<float>(milivoltsOV) / maxOV * fullCode;
    }

    MAP_I2CMasterSlaveAddrSet(I2C, i2cAddr, false);
    MAP_I2CMasterDataPut(I2C, code);
    MAP_I2CMasterControl(I2C, I2C_MASTER_CMD_SINGLE_SEND);

    while (MAP_I2CMasterBusy(I2C)) {}
}

#endif /* SIPMOV_H */
