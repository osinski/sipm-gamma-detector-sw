/*
 * mca.cc
 * Copyright (C) 2021 Marcin Osiński <osinski.marcin.r@gmail.com>
 *
 * Distributed under terms of the MIT license.
 */

#include "mca.h"

#include <string>

#include "../tivaWare/driverlib/ssi.h"
#include "../tivaWare/driverlib/timer.h"
#include "../tivaWare/driverlib/gpio.h"
#include "../tivaWare/driverlib/interrupt.h"
#include "../tivaWare/driverlib/pin_map.h"
#include "../tivaWare/driverlib/rom.h"
#include "../tivaWare/driverlib/rom_map.h"
#include "../tivaWare/inc/hw_memmap.h"
#include "../tivaWare/inc/hw_ssi.h"
#include "../tivaWare/inc/hw_types.h"

#include "../freeRTOS/include/semphr.h"

#include "uart.h"

extern UART uart;

namespace MCA {
    constexpr static auto i2c = I2C3_BASE;
    constexpr static auto i2cAddr = 0b010'1111;
    constexpr static auto fullCode = 256;
    constexpr static auto comparatorPort = GPIO_PORTM_BASE;
    constexpr static auto comparatorPin = GPIO_PIN_0;
    constexpr static auto pdResetPort = GPIO_PORTA_BASE;
    constexpr static auto pdResetPin = GPIO_PIN_4;
    constexpr static auto pdResetTimer = TIMER2_BASE;
    constexpr static auto pdResetGpioFunction = GPIO_PA4_T2CCP0;
    constexpr static auto spi = SSI3_BASE;
    constexpr static auto timer = TIMER2_BASE;

    static MCA::SpectrogramData spectrogram;
    volatile static std::uint_fast16_t peakHeightCode;
    static SemaphoreHandle_t semaphorePDTrigger;
    static SemaphoreHandle_t semaphorePDResetRelease;

    inline void
    enable()
    {
        MAP_GPIOPinTypeGPIOOutput(GPIO_PORTA_BASE, GPIO_PIN_5);

        MAP_GPIOPinTypeGPIOInput(comparatorPort, comparatorPin);
        MAP_GPIOPadConfigSet(comparatorPort, comparatorPin, GPIO_STRENGTH_2MA,
                                GPIO_PIN_TYPE_STD_WPU); // for testing
        MAP_GPIOIntEnable(comparatorPort, comparatorPin);
        MAP_GPIOIntTypeSet(comparatorPort, comparatorPin, GPIO_RISING_EDGE); // for testing

        MAP_GPIOPinTypeTimer(pdResetPort, pdResetPin);
        MAP_GPIOPinConfigure(pdResetGpioFunction);

        MAP_TimerConfigure(timer, TIMER_CFG_SPLIT_PAIR | 
                                  TIMER_CFG_A_ONE_SHOT_PWM |
                                  //TIMER_CFG_A_ACT_CLRSETTO |
                                  TIMER_CFG_B_ONE_SHOT_UP |
                                  TIMER_CFG_B_ACT_NONE);
        //MAP_TimerControlLevel(timer, TIMER_A, false);

        MAP_TimerLoadSet(timer, TIMER_A, 1200);
        MAP_TimerMatchSet(timer, TIMER_A, 0);
        MAP_TimerLoadSet(timer, TIMER_B, 1200);

        MAP_SSIEnable(spi);

        MAP_TimerIntEnable(timer, TIMER_TIMB_TIMEOUT);
        MAP_IntPrioritySet(INT_TIMER2B, 0xE0);
        MAP_IntEnable(INT_TIMER2B);

        MAP_IntPrioritySet(INT_GPIOM, 0xE0);
        MAP_IntEnable(INT_GPIOM);
    }

    inline void
    disable()
    {
        MAP_GPIOIntDisable(comparatorPort, comparatorPin);
        MAP_IntDisable(INT_GPIOM);
        MAP_IntDisable(INT_TIMER2A);
        MAP_IntDisable(INT_SSI3);

        MAP_GPIOPinTypeGPIOInput(pdResetPort, pdResetPin);

        MAP_SSIDisable(spi);

        MAP_TimerDisable(timer, TIMER_A);
    }

    inline std::uint_fast16_t
    getADCcode()
    {
        volatile uint32_t adcCode = 0;

        HWREG(spi + SSI_O_DR) = 0xffff;

        //slower but safer to check if data is ready
        while(!(HWREG(spi + SSI_O_SR) & SSI_SR_RNE)) {}
        
        adcCode = HWREG(spi + SSI_O_DR);

        return adcCode >> 2;
    }

    inline std::uint_fast16_t
    getPeakHeight(std::uint_fast16_t adcCode)
    {
        constexpr auto vRef = 2500;
        constexpr auto adcResBits = 12;
        constexpr auto codeFullRange = (1 << adcResBits) - 1;

        //return adcCode >> 1;
        return static_cast<float>(adcCode) / codeFullRange * vRef;
    }


    inline void
    resetPeakDetector()
    {
        MAP_TimerEnable(timer, TIMER_A);
        MAP_TimerEnable(timer, TIMER_B);
    }

    inline void
    updateSpectrogram(SpectrogramData& spectrogram, std::uint_fast16_t code)
    {
        constexpr auto adcResBits = 12;
        constexpr auto codeFullRange = (1 << adcResBits) - 1;

        std::size_t channelToUpdate = code*spectrogram.size()/codeFullRange;

        //uart.sendString(std::to_string(code).c_str());
        //uart.sendString(" - ");
        //uart.sendString(std::to_string(channelToUpdate).c_str());
        //uart.sendString("\r\n");

        if (channelToUpdate > spectrogram.size() - 1) {
            channelToUpdate = spectrogram.size() - 1;
        }

        spectrogram.at(channelToUpdate)++;
    }
}


extern "C" {

QueueHandle_t queueMCASpectrogram;
QueueHandle_t queueMCAMsgs;

void
taskMCAConfig(void*)
{
    static MCA::MessageType msgMCA;

    queueMCAMsgs = xQueueCreate(1, sizeof(MCA::MessageType));

    while (true) {
        xQueueReceive(queueMCAMsgs, &msgMCA, portMAX_DELAY);
        switch (msgMCA) {
        case MCA::MessageType::ENABLE:
        {
            MCA::enable();
            break;
        }
        case MCA::MessageType::DISABLE:
        {
            MCA::disable();

            for (auto& val : MCA::spectrogram) {
                uart.sendString(std::to_string(val).c_str());
                uart.sendString(", ");
            }
            uart.sendString("\r\n");
            break;
        }
        }
    }
}

void
taskMCA(void*)
{

    auto peakHeight = 0;

    queueMCASpectrogram = xQueueCreate(1, sizeof(MCA::SpectrogramData));
    MCA::semaphorePDTrigger = xSemaphoreCreateBinary();
    MCA::semaphorePDResetRelease = xSemaphoreCreateBinary();

    MCA::spectrogram.fill(0);

    while (true) {
        xSemaphoreTake(MCA::semaphorePDTrigger, portMAX_DELAY);
        //peakHeight = MCA::getPeakHeight(MCA::peakHeightCode);
        //uart.sendString("Peak Height: ");
        //uart.sendString(std::to_string(peakHeight).c_str());
        //uart.sendString("\r\n");
        MCA::updateSpectrogram(MCA::spectrogram, MCA::peakHeightCode);
        xQueueSend(queueMCASpectrogram, &MCA::spectrogram, 0);
        xSemaphoreTake(MCA::semaphorePDResetRelease, portMAX_DELAY);
        MAP_IntEnable(INT_GPIOM);
    }
}

void
ISR_GPIOM(void)
{
    auto xHigherPriorityTaskWoken = pdFALSE;

    auto intStatus = GPIOIntStatus(GPIO_PORTM_BASE, GPIO_INT_PIN_0);
    GPIOIntClear(GPIO_PORTM_BASE, GPIO_INT_PIN_0);

    MCA::peakHeightCode = MCA::getADCcode();
    MCA::resetPeakDetector();

    xSemaphoreGiveFromISR(MCA::semaphorePDTrigger, &xHigherPriorityTaskWoken);

    if (xHigherPriorityTaskWoken != pdFALSE) {
        portEND_SWITCHING_ISR(xHigherPriorityTaskWoken);
    }

    MAP_IntDisable(INT_GPIOM);
}

void
ISR_SSI3(void)
{
    auto xHigherPriorityTaskWoken = pdFALSE;
    auto intStatus = MAP_SSIIntStatus(MCA::spi, true);

    if (intStatus & SSI_TXEOT) {
        MAP_SSIIntClear(MCA::spi, SSI_TXEOT);


        if (xHigherPriorityTaskWoken != pdFALSE) {
            portEND_SWITCHING_ISR(xHigherPriorityTaskWoken);
        }
    } else {
        MAP_SSIIntClear(MCA::spi, intStatus);
    }
}

void
ISR_Timer2B(void)
{
    auto xHigherPriorityTaskWoken = pdFALSE;
    auto intStatus = MAP_TimerIntStatus(MCA::timer, true);
    MAP_TimerIntClear(MCA::timer, intStatus);

    xSemaphoreGiveFromISR(MCA::semaphorePDResetRelease,
                        &xHigherPriorityTaskWoken);

    if (xHigherPriorityTaskWoken != pdFALSE) {
        portEND_SWITCHING_ISR(xHigherPriorityTaskWoken);
    }

}

}
