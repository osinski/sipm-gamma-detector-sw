/*
 * button.h
 * Copyright (C) 2021 Marcin Osiński <osinski.marcin.r@gmail.com>
 *
 * Distributed under terms of the MIT license.
 */

#ifndef BUTTON_H
#define BUTTON_H

#include <stdint.h>

#include "../tivaWare/driverlib/gpio.h"
#include "../tivaWare/driverlib/rom_map.h"
#include "../tivaWare/inc/hw_memmap.h"
#include "../tivaWare/inc/tm4c129encpdt.h"

struct Button {
    Button(const uint32_t buttonPort, const uint32_t buttonPin)
      : port{ buttonPort }
      , pin{ buttonPin }
    {
    }

    const uint32_t port;
    const uint32_t pin;

    void
    setup()
    {
        MAP_GPIOPinTypeGPIOInput(port, pin);
        MAP_GPIOPadConfigSet(port, pin, GPIO_STRENGTH_2MA, GPIO_PIN_TYPE_STD_WPU);
        MAP_GPIOIntTypeSet(port, pin, GPIO_FALLING_EDGE);
        MAP_GPIOIntEnable(port, pin);
    }
};

#endif /* BUTTON_H */
