/*
 * sca.h
 * Copyright (C) 2021 Marcin Osiński <osinski.marcin.r@gmail.com>
 *
 * Distributed under terms of the MIT license.
 */

#ifndef SCA_H
#define SCA_H

#include <cstdint>
#include <utility>

#include "../freeRTOS/include/FreeRTOS.h"
#include "../freeRTOS/include/queue.h"

namespace SCA {

enum class MessageType
{
    NEW_LLD,
    NEW_ULD,
    ENABLE,
    DISABLE,
    SEND_DATA,
};

using Message = std::pair<MessageType, std::uint_fast16_t>;
using Data = std::pair<volatile std::uint_fast64_t, volatile std::uint_fast64_t>;

}

extern "C" {

extern QueueHandle_t queueSCAMsgs;
extern QueueHandle_t queueSCAData;

void
taskSCA(void*);

void
taskSCAConfig(void*);

}
#endif /* SCA_H */
