/*
 * httpd_server.cc
 * Copyright (C) 2021 Marcin Osiński <osinski.marcin.r@gmail.com>
 *
 * Distributed under terms of the MIT license.
 */

#include "httpd_server.h"

#include <array>
#include <charconv>
#include <cstdio>
#include <cstring>
#include <optional>
#include <string>
#include <string_view>

#include "../tivaWare/lwip/src/include/lwip/api.h"

#include "../freeRTOS/include/FreeRTOS.h"
#include "../freeRTOS/include/queue.h"
#include "../freeRTOS/include/stream_buffer.h"
#include "../freeRTOS/include/task.h"

#include "fs.h"
#include "fsdata.c"
#include "mca.h"
#include "sca.h"
#include "sipmOV.h"
#include "uart.h"

struct getHTTPHeader {
    std::string_view request;
    std::string_view versionHTTP;

    constexpr static std::string_view defaultFile = "/index.html";

    getHTTPHeader() = default;

    static std::optional<getHTTPHeader>
    buildHTTP(const std::string_view data)
    {
        getHTTPHeader hdr;
        auto nlinePos = data.find("\n");

        if (nlinePos == std::string::npos) {
            return std::nullopt;
        }

        auto fline = data.substr(0, nlinePos);

        if (!fline.starts_with("GET")) {
            return std::nullopt;
        }

        auto filePosBeg = fline.find("/");
        auto filePosEnd = fline.find(" ", filePosBeg);

        if (filePosBeg == std::string::npos ||
            filePosEnd == std::string::npos) {
            return std::nullopt;
        }

        hdr.request = fline.substr(filePosBeg, filePosEnd - filePosBeg);
        hdr.versionHTTP = fline.substr(filePosEnd, nlinePos - filePosEnd - 1);

        if (hdr.request.size() == 1 and *hdr.request.data() == '/') {
            hdr.request = defaultFile;
        }

        return hdr;
    }

    [[nodiscard]] const std::string_view
    getParam(const char separator) const
    {
        const auto sepPos = request.find(separator) + 1;
        const auto endPos = request.find(' ', sepPos);
        return request.substr(sepPos, endPos - sepPos);
    }

    [[nodiscard]] std::optional<std::uint_fast16_t>
    getParam(const std::string_view paramStr) const
    {
        std::uint_fast16_t param;

        auto result = std::from_chars(
          paramStr.data(), paramStr.data() + paramStr.size(), param);

        if (result.ec != std::errc()) {
            return std::nullopt;
        } else {
            return param;
        }
    }

    [[nodiscard]] bool
    getState(const std::string_view paramStr) const
    {
        if (paramStr == std::string_view("ON")) {
            return true;
        } else if (paramStr == std::string_view("OFF")) {
            return false;
        } else {
            return false;
        }
    }
};

static void
http_server_netconn_serve(struct netconn* conn);

extern "C" void
taskHTTPServer(void*)
{
    struct netconn *conn, *newconn;
    err_t err;
    extern UART uart;

    /* Create a new TCP connection handle */
    /* Bind to port 80 (HTTP) with default IP address */
#if LWIP_IPV6
    netconn_bind(conn, IP6_ADDR_ANY, 80);
#else  /* LWIP_IPV6 */
    conn = netconn_new(NETCONN_TCP);
    netconn_bind(conn, IP_ADDR_ANY, 80);
#endif /* LWIP_IPV6 */
    if (conn == NULL)
        while (1)
            ;

    /* Put the connection into LISTEN state */
    netconn_listen(conn);

    do {
        err = netconn_accept(conn, &newconn);
        if (err == ERR_OK) {
            http_server_netconn_serve(newconn);
            netconn_delete(newconn);
        }
    } while (err == ERR_OK);

    while (1)
        ;
    netconn_close(conn);
    netconn_delete(conn);
}

template<typename T, std::size_t U>
static const std::string
makePrintable(const std::array<T, U>& data)
{
    std::string printable;

    for (auto const& val : data) {
        if (&val != &data[0]) {
            printable += std::string(",");
        }
        printable += std::to_string(val);
    }

    return printable;
}

/** Serve one HTTP connection accepted in the http thread */
static void
http_server_netconn_serve(struct netconn* conn)
{
    struct netbuf* inbuf;
    char* buf;
    u16_t buflen;
    err_t err;

    extern UART uart;

    extern QueueHandle_t queueSCAMsgs;
    static SCA::Message msgSCA;
    extern QueueHandle_t queueSCAData;
    static SCA::Data dataSCA;

    extern QueueHandle_t queueMCAMsgs;
    static MCA::MessageType msgMCA;
    extern QueueHandle_t queueMCASpectrogram;
    static MCA::SpectrogramData dataMCA;

    xQueueReceive(queueMCASpectrogram, &dataMCA, 0);

    /* Read the data from the port, blocking if nothing yet there.
     We assume the request (the part we care about) is in one netbuf */
    err = netconn_recv(conn, &inbuf);

    if (err == ERR_OK) {
        netbuf_data(inbuf, (void**)&buf, &buflen);

        auto hOpt = getHTTPHeader::buildHTTP({ buf, buflen });

        if (hOpt.has_value()) {
            auto h = *hOpt;

            auto page = h.request;

            uart.flushBuffer(page.data(), page.size());
            uart.sendString("\r\n");

            static fs_file file;

            auto notFile = fs_open(&file, (std::string(page) + '\0').c_str());

            if (notFile) {
                if (page.starts_with("/newOV")) {
                    auto paramStr = h.getParam('=');
                    auto param = *h.getParam(paramStr);

                    uart.sendString("...OK, new overvoltage is ");
                    uart.flushBuffer(paramStr.data(), paramStr.size());
                    uart.sendString("\r\n");

                    setSiPMOvervoltage(param);

                    netconn_write(conn, "ACK", 3, NETCONN_NOCOPY);
                } else if (page.starts_with("/newLLD")) {
                    auto paramStr = h.getParam('=');
                    auto param = *h.getParam(paramStr);

                    uart.sendString("...OK, new LLD is ");
                    uart.flushBuffer(paramStr.data(), paramStr.size());
                    uart.sendString("\r\n");
                    
                    msgSCA.first = SCA::MessageType::NEW_LLD;
                    msgSCA.second = param;
                    xQueueSend(queueSCAMsgs, &msgSCA, 0);

                    netconn_write(conn, "ACK", 3, NETCONN_NOCOPY);
                } else if (page.starts_with("/newULD")) {
                    auto paramStr = h.getParam('=');
                    auto param = *h.getParam(paramStr);

                    uart.sendString("...OK, new ULD is ");
                    uart.flushBuffer(paramStr.data(), paramStr.size());
                    uart.sendString("\r\n");

                    msgSCA.first = SCA::MessageType::NEW_ULD;
                    msgSCA.second = param;
                    xQueueSend(queueSCAMsgs, &msgSCA, 0);

                    netconn_write(conn, "ACK", 3, NETCONN_NOCOPY);
                } else if (page.starts_with("/SCA")) {
                    auto paramStr = h.getParam('=');
                    auto state = h.getState(paramStr);

                    uart.sendString("...OK, SCA state is ");
                    uart.flushBuffer(paramStr.data(), paramStr.size());
                    uart.sendString("\r\n");

                    state ? msgSCA.first = SCA::MessageType::ENABLE :
                            msgSCA.first = SCA::MessageType::DISABLE;
                    xQueueSend(queueSCAMsgs, &msgSCA, 0);

                    netconn_write(conn, "ACK", 3, NETCONN_NOCOPY);
                } else if (page.starts_with("/MCA")) {
                    auto paramStr = h.getParam('=');
                    auto state = h.getState(paramStr);

                    uart.sendString("...OK, MCA state is ");
                    uart.flushBuffer(paramStr.data(), paramStr.size());
                    uart.sendString("\r\n");

                    state ? msgMCA = MCA::MessageType::ENABLE :
                            msgMCA = MCA::MessageType::DISABLE;
                    xQueueSend(queueMCAMsgs, &msgMCA, 0);

                    netconn_write(conn, "ACK", 3, NETCONN_NOCOPY);
                } else if (page.starts_with("/DataSCA")) {
                    msgSCA.first = SCA::MessageType::SEND_DATA;
                    xQueueSend(queueSCAMsgs, &msgSCA, 0);
                    xQueueReceive(queueSCAData, &dataSCA, portMAX_DELAY);
                    
                    auto response = 
                        makePrintable(std::array{dataSCA.first, dataSCA.second});
                    uart.sendString(response.c_str());
                    uart.sendString("\r\n");

                    netconn_write(conn, response.c_str(), response.length(),
                            NETCONN_NOCOPY);
                } else if (page.starts_with("/DataMCA")) {
                    xQueueReceive(queueMCASpectrogram, &dataMCA, 0);

                    auto response = makePrintable(dataMCA);

                    netconn_write(conn, response.c_str(), response.length(),
                            NETCONN_NOCOPY);
                } else {
                    netconn_write(conn, "NACK", 4, NETCONN_NOCOPY);
                }
            } else {
                netconn_write(conn, file.data, file.len, NETCONN_NOCOPY);
            }

            fs_close(&file);
        }
    }
    /* Close the connection (server closes in HTTP) */
    netconn_close(conn);

    /* Delete the buffer (netconn_recv gives us ownership,
     so we have to make sure to deallocate the buffer) */
    netbuf_delete(inbuf);
}



