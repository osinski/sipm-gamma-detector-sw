/*
 * main.cc
 * Copyright (C) 2021 Marcin Osiński <osinski.marcin.r@gmail.com>
 *
 * Distributed under terms of the MIT license.
 */

#include <array>
#include <stdint.h>
#include <string.h>
#include <utility>

#include "../freeRTOS/include/FreeRTOS.h"
#include "../freeRTOS/include/queue.h"
#include "../freeRTOS/include/semphr.h"
#include "../freeRTOS/include/task.h"
#include "FreeRTOSConfig.h"

#include "../tivaWare/driverlib/flash.h"
#include "../tivaWare/driverlib/gpio.h"
#include "../tivaWare/driverlib/i2c.h"
#include "../tivaWare/driverlib/ssi.h"
#include "../tivaWare/driverlib/interrupt.h"
#include "../tivaWare/driverlib/pin_map.h"
#include "../tivaWare/driverlib/rom.h"
#include "../tivaWare/driverlib/rom_map.h"
#include "../tivaWare/driverlib/sysctl.h"
#include "../tivaWare/driverlib/timer.h"
#include "../tivaWare/driverlib/uart.h"
#include "../tivaWare/inc/hw_gpio.h"
#include "../tivaWare/inc/hw_memmap.h"
#include "../tivaWare/inc/hw_types.h"
#include "../tivaWare/inc/tm4c129encpdt.h"
#include "../tivaWare/utils/lwiplib.h"

#include "../tivaWare/lwip/src/include/lwip/ip4_addr.h"

#include "button.h"
#include "cmdline.h"
#include "httpd_server.h"
#include "led.h"
#include "logger.h"
#include "uart.h"
#include "mca.h"
#include "sca.h"

void
configHW();

UART uart(UART0_BASE, 115200);

Button button1(GPIO_PORTJ_BASE, GPIO_PIN_0);
Button button2(GPIO_PORTJ_BASE, GPIO_PIN_1);

QueueHandle_t queueLogger;
QueueHandle_t queueCmdLine;

extern "C" int
main()
{
    configHW();

    uart.sendString(ANSI_EscapeCodes::Controls::clearEntireScreen);
    uart.sendString(ANSI_EscapeCodes::Controls::cursorScreenOrigins);
    uart.sendString(ANSI_EscapeCodes::Decorations::bold);
    uart.sendString(ANSI_EscapeCodes::Colors::brightCyan);
    uart.sendString("EK-TM4C129EXL Devboard RTOS demo\r\n");
    uart.sendString(ANSI_EscapeCodes::reset);

    queueLogger = xQueueCreate(1, sizeof(LoggingEvents));
    queueCmdLine = xQueueCreate(1, sizeof(char));

    xTaskCreate(taskLEDs, "LEDs", 100, NULL, 1, NULL);
    xTaskCreate(taskLogger, "Logger", 100, NULL, 2, NULL);
    xTaskCreate(taskCmdLine, "CmdLine", 1000, NULL, 3, NULL);
    xTaskCreate(taskHTTPServer, "HTTPserver", 1000, NULL, 1, NULL);
    xTaskCreate(taskSCA, "SCA", 100, NULL, 1, NULL);
    xTaskCreate(taskSCAConfig, "SCA config", 100, NULL, 1, NULL);
    xTaskCreate(taskMCA, "MCA", 100, NULL, 2, NULL);
    xTaskCreate(taskMCAConfig, "MCA Config", 100, NULL, 1, NULL);
    uart.sendString("Scheduler starting\r\n");
    vTaskStartScheduler();

    while (1) {};
}

void
configHW()
{
    MAP_SysCtlMOSCConfigSet(SYSCTL_MOSC_HIGHFREQ);
    MAP_SysCtlClockFreqSet((SYSCTL_XTAL_25MHZ | SYSCTL_OSC_MAIN |
                            SYSCTL_USE_PLL | SYSCTL_CFG_VCO_240),
                           configCPU_CLOCK_HZ);

    MAP_SysCtlPeripheralEnable(SYSCTL_PERIPH_GPIOA);
    while (!MAP_SysCtlPeripheralReady(SYSCTL_PERIPH_GPIOA)) {}
    MAP_SysCtlPeripheralEnable(SYSCTL_PERIPH_GPIOB);
    while (!MAP_SysCtlPeripheralReady(SYSCTL_PERIPH_GPIOB)) {}
    MAP_SysCtlPeripheralEnable(SYSCTL_PERIPH_GPIOC);
    while (!MAP_SysCtlPeripheralReady(SYSCTL_PERIPH_GPIOC)) {}
    MAP_SysCtlPeripheralEnable(SYSCTL_PERIPH_GPIOD);
    while (!MAP_SysCtlPeripheralReady(SYSCTL_PERIPH_GPIOD)) {}
    MAP_SysCtlPeripheralEnable(SYSCTL_PERIPH_GPIOE);
    while (!MAP_SysCtlPeripheralReady(SYSCTL_PERIPH_GPIOE)) {}
    MAP_SysCtlPeripheralEnable(SYSCTL_PERIPH_GPIOF);
    while (!MAP_SysCtlPeripheralReady(SYSCTL_PERIPH_GPIOF)) {}
    MAP_SysCtlPeripheralEnable(SYSCTL_PERIPH_GPIOK);
    while (!MAP_SysCtlPeripheralReady(SYSCTL_PERIPH_GPIOK)) {}
    MAP_SysCtlPeripheralEnable(SYSCTL_PERIPH_GPIOJ);
    while (!MAP_SysCtlPeripheralReady(SYSCTL_PERIPH_GPIOJ)) {}
    MAP_SysCtlPeripheralEnable(SYSCTL_PERIPH_GPIOL);
    while (!MAP_SysCtlPeripheralReady(SYSCTL_PERIPH_GPIOL)) {}
    MAP_SysCtlPeripheralEnable(SYSCTL_PERIPH_GPIOM);
    while (!MAP_SysCtlPeripheralReady(SYSCTL_PERIPH_GPIOM)) {}
    MAP_SysCtlPeripheralEnable(SYSCTL_PERIPH_GPION);
    while (!MAP_SysCtlPeripheralReady(SYSCTL_PERIPH_GPION)) {}
    MAP_SysCtlPeripheralEnable(SYSCTL_PERIPH_GPIOP);
    while (!MAP_SysCtlPeripheralReady(SYSCTL_PERIPH_GPIOP)) {}
    MAP_SysCtlPeripheralEnable(SYSCTL_PERIPH_GPIOQ);
    while (!MAP_SysCtlPeripheralReady(SYSCTL_PERIPH_GPIOQ)) {}

    MAP_SysCtlPeripheralEnable(SYSCTL_PERIPH_UART0);
    while (!MAP_SysCtlPeripheralReady(SYSCTL_PERIPH_UART0)) {}
    MAP_SysCtlPeripheralEnable(SYSCTL_PERIPH_I2C3);
    while (!MAP_SysCtlPeripheralReady(SYSCTL_PERIPH_I2C3)) {}
    MAP_SysCtlPeripheralEnable(SYSCTL_PERIPH_TIMER2);
    while (!MAP_SysCtlPeripheralReady(SYSCTL_PERIPH_TIMER2)) {}
    MAP_SysCtlPeripheralEnable(SYSCTL_PERIPH_SSI3);
    while (!MAP_SysCtlPeripheralReady(SYSCTL_PERIPH_SSI3)) {}

    for (auto& led : userLEDs) { led.setup(); }

    button1.setup();
    button2.setup();

    MAP_IntPrioritySet(INT_GPIOJ, 0xE0);
    MAP_IntEnable(INT_GPIOJ);
    MAP_IntPrioritySet(INT_UART0, 0xE0);
    MAP_IntEnable(INT_UART0);
    MAP_IntMasterEnable();

    MAP_GPIOPinTypeUART(GPIO_PORTA_BASE, GPIO_PIN_0 | GPIO_PIN_1);
    MAP_GPIOPinConfigure(GPIO_PA0_U0RX);
    MAP_GPIOPinConfigure(GPIO_PA1_U0TX);
    uart.setup();

    MAP_GPIOPinConfigure(GPIO_PK4_I2C3SCL);
    MAP_GPIOPinConfigure(GPIO_PK5_I2C3SDA);
    MAP_GPIOPinTypeI2CSCL(GPIO_PORTK_BASE, GPIO_PIN_4);
    MAP_GPIOPinTypeI2C(GPIO_PORTK_BASE, GPIO_PIN_5);
    MAP_I2CMasterInitExpClk(I2C3_BASE, configCPU_CLOCK_HZ, false);

    MAP_GPIOPinConfigure(GPIO_PQ0_SSI3CLK);
    MAP_GPIOPinConfigure(GPIO_PQ1_SSI3FSS);
    MAP_GPIOPinConfigure(GPIO_PQ3_SSI3XDAT1);
    MAP_GPIOPinTypeSSI(GPIO_PORTQ_BASE, GPIO_PIN_0 | GPIO_PIN_1 | GPIO_PIN_3);
    MAP_SSIConfigSetExpClk(SSI3_BASE, configCPU_CLOCK_HZ, SSI_FRF_MOTO_MODE_3,
                            SSI_MODE_MASTER, 48'000'000, 16);

    uint32_t user0, user1;
    uint8_t MACArray[8];
    FlashUserGet(&user0, &user1);

    if ((user0 == 0xffffffff) || (user1 == 0xffffffff)) {
        uart.sendString("No MAC programmed\r\n");

        while (true) {}
    }

    MACArray[0] = ((user0 >> 0) & 0xff);
    MACArray[1] = ((user0 >> 8) & 0xff);
    MACArray[2] = ((user0 >> 16) & 0xff);
    MACArray[3] = ((user1 >> 0) & 0xff);
    MACArray[4] = ((user1 >> 8) & 0xff);
    MACArray[5] = ((user1 >> 16) & 0xff);

    uint32_t ipAddr = 192 << 24 | 168 << 16 | 24 << 8 | 24 << 0;
    uint32_t ipGW = 192 << 24 | 168 << 16 | 24 << 8 | 1 << 0;
    uint32_t ipMask = 255 << 24 | 255 << 16 | 255 << 8 | 0 << 0;

    lwIPInit(
      configCPU_CLOCK_HZ, MACArray, ipAddr, ipMask, ipGW, IPADDR_USE_STATIC);
}

extern "C" {

void
ISR_GPIOJ(void)
{
    static constexpr auto debounceMS = 200;

    static uint64_t lastISR = 0;
    TickType_t ticksISR = xTaskGetTickCount();

    auto xHigherPriorityTaskWoken = pdFALSE;
    LoggingEvents event;

    auto intStatus =
      GPIOIntStatus(GPIO_PORTJ_BASE, GPIO_INT_PIN_0 | GPIO_INT_PIN_1);
    GPIOIntClear(GPIO_PORTJ_BASE, GPIO_INT_PIN_0 | GPIO_INT_PIN_1);

    if (ticksISR - lastISR > pdMS_TO_TICKS(debounceMS)) {
        switch (intStatus) {
            case GPIO_INT_PIN_0:
                event = Button1;
                xQueueSendFromISR(
                  queueLogger, &event, &xHigherPriorityTaskWoken);
                break;

            case GPIO_INT_PIN_1:
                event = Button2;
                xQueueSendFromISR(
                  queueLogger, &event, &xHigherPriorityTaskWoken);
                break;

            default:
                break;
        };

        if (xHigherPriorityTaskWoken != pdFALSE) {
            portEND_SWITCHING_ISR(xHigherPriorityTaskWoken);
        }
    }

    lastISR = ticksISR;
}

void
ISR_UART0(void)
{
    char recvChar;
    auto xHigherPriorityTaskWoken = pdFALSE;

    auto intStatus = UARTIntStatus(UART0_BASE, false);
    UARTIntClear(UART0_BASE, intStatus);

    switch (intStatus) {
        case UART_INT_RT:
            recvChar = UARTCharGet(UART0_BASE);
            xQueueSendFromISR(
              queueCmdLine, &recvChar, &xHigherPriorityTaskWoken);
            break;

        default:
            break;
    }

    if (xHigherPriorityTaskWoken != pdFALSE) {
        portEND_SWITCHING_ISR(xHigherPriorityTaskWoken);
    }
}
}
